package gr.home.teiath.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Login", urlPatterns =
{
    "/Login"
})
public class Login extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try
        {
            String Username = request.getParameter("Username");
            String Password = request.getParameter("Password");

            boolean LogedIn = Login(Username, Password);

            if (LogedIn)
            {
                HttpSession session = request.getSession();
                session.setAttribute("Username", Username);
            }


            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            if (LogedIn)
            {
                out.println("<h1>Welkome " + Username + "</h1>");
            } else
            {
                out.println("<h1>Invalid password or username. </h1>");
            }

            out.println("</body>");
            out.println("</html>");
        } finally
        {
            out.close();
        }
    }

    private boolean Login(String username, String password)
    {
        // Check for null parameters
        if (username == null || password == null)
        {
            return false;
        }

        // Check for empty parameters
        if (!(username.length() > 0 && password.length() > 0))
        {
            return false;
        }

        //Find if username already exists
        for (Iterator<Cookie> i = CookiesTempDB.test.iterator(); i.hasNext();)
        {
            Cookie item = i.next();
            String Name = item.getName();
            String Psw = item.getValue();

            if (Name.equals(username) && Psw.equals(password))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
