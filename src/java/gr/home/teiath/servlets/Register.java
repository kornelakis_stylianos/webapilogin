/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.home.teiath.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Stelios
 */
@WebServlet(name = "Register", urlPatterns =
{
    "/Register"
})
public class Register extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {

        String Username = request.getParameter("Username");
        String Password = request.getParameter("Password");

        boolean returnValue = Register(Username, Password);

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try
        {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Register</title>");
            out.println("</head>");
            out.println("<body>");
            
            if (returnValue)
            {
                out.println("<h1>User: " + Username + " successfully registered.</h1>");
            }
            else
            {
                out.println("<h1>Registration Failed, user already exists or empty parameters</h1>");
            }
            out.println("</body>");
            out.println("</html>");
        } finally
        {
            out.close();
        }
    }

    private boolean Register(String username, String password)
    {
        // Check for null parameters
        if (username == null || password == null)
        {
            return false;
        }

        // Check for empty parameters
        if (!(username.length() > 0 && password.length() > 0))
        {
            return false;
        }

        //Find if username already exists
        for (Iterator<Cookie> i = CookiesTempDB.test.iterator(); i.hasNext();)
        {
            Cookie item = i.next();
            String Name = item.getName();

            if (Name.equals(username))
            {
                return false;
            }
        }

        //Create the Cookie
        Cookie CookieToAdd = new Cookie(username, password);

        return CookiesTempDB.test.add(CookieToAdd);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
